require: slotfilling/slotFilling.sc
  module = sys.zb-common

require: common.js
    module = sys.zb-common
require: functions.js

patterns:
    $InputNumber = $regexp<\d+> 

theme: /

    state: Правила
        q!: $regex</start>
        intent!: /Давай поиграем
        a: Сыграем в игру быки и коровы. Я загадываю четырехзначное число, все цифры в числе различны, ты будешь угадывать, а я - называть количество быков и коров. Количество коров - это количество угаданных цифр не на своих позициях, а быков - на своих. Начнём?
        go!: /Правила/Согласен?

        state: Согласен?
            
            state: Да
                intent: /Согласие
                a: Загадал! Угадывай
                go!: /Игра

            state: Нет
                intent!: /Несогласие
                a: Ну и ладно! Если передумаешь - скажи "давай поиграем"

    state: Игра
        # сгенерируем случайное число и перейдем в стейт /Проверка
        script:
            $session.attemptCounter = 0; // счетчик попыток
            
            if (testMode()) {
                $session.botNumber = 1230; // для тестирования
                $reactions.answer("Загадано {{$session.botNumber}}");
            } else {
                $session.botNumber = getGoalNumber();
                //$reactions.answer("Загадано {{$session.botNumber}}");
                $reactions.transition("/Проверка");
            };

    state: Проверка
        q: * $InputNumber *
        script:
            # сохраняем введенное пользователем число
            var num = $parseTree._InputNumber;  
            if (num) {
                // log(toPrettyString($parseTree));
                var arr_user= toArray(num);
    
                $session.attemptCounter++;

                // проверяем, корректно ли число, если нет, выводим соответсвующее сообщение об ошибке
                var notValid = numberIsNotValid(arr_user);
                if (notValid) {
                    $reactions.answer(notValid);
                } else {
                    var arr_bot = toArray($session.botNumber);
                    var values = getBullsAndCows(arr_bot, arr_user);
                    $temp.bulls = values[0];
                    $temp.cows = values[1];
                    
                // проверяем угадал ли пользователь загаданное число и выводим соответствующую реакцию
                    if (num == $session.botNumber) {
                        var answer = "Ты выиграл с " + $session.attemptCounter + " " + $caila.conform($caila.inflect("попытки", ["gent"]), $session.attemptCounter) + ". Сыграем еще раз?"
                        $reactions.answer(answer);
                        $reactions.transition("/Правила/Согласен?");
                    }
                    else if (num != $session.botNumber) {
                            if ($temp.bulls == 0) $temp.bulls = "нет";
                            if ($temp.cows == 0) $temp.cows = "нет";
                            var answer = $temp.bulls + " " + $caila.conform("бык", $temp.bulls) + ", " + $temp.cows + " " +  $caila.conform("корова", $temp.cows)
                            $reactions.answer(capitalize(answer));
                    };
                };
            };
       
    state: Пока
        intent!: /Пока
        a: До встречи! Захочешь поиграть, напиши "давай поиграем" :)

        
    state: NoMatch || noContext = true
        event!: noMatch
        random:
            a: Я не понял.
            a: Что вы имеете в виду?
            a: Ничего не пойму
            
            
            
            
