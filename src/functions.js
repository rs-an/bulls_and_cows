function getGoalNumber() {
    var arr = [];
    while (arr.length < 4) {
        var num = getRandom();
        if ((arr.indexOf(num)==-1) && (arr.indexOf(0)!=0)) {
            arr.push(num);
        };
    };
    return Number(arr.join(""));
};

function getRandom() {
    return Math.floor(Math.random() * 10);
};

function toArray(inputNumber) {
    var output = [];
    var sNumber;
    if (typeof inputNumber == 'number') {
        sNumber = inputNumber.toString();
    } else if (typeof inputNumber == 'string') 
        sNumber = inputNumber;

    for (var i=0, len=sNumber.length; i<len; i++)
        output.push(+sNumber.charAt(i));
    return output;
};

function getBullsAndCows(arr1, arr2) {
    var bulls = 0;
    var cows = 0;
    for (var i=0, len=arr1.length; i<len; i++) {
        if (arr1[i] == arr2[i]) {
            bulls++;
        } else if (arr2.indexOf(arr1[i])!=-1)
            cows++;
    };
    return [bulls, cows];
};

function numberIsNotValid(arr) {
    function onlyUnique(value, index, self) {
        return self.indexOf(value) === index;
    };
  
    var uniqueValues = arr.filter(onlyUnique);
    var n_unique = uniqueValues.length;

    if (arr.length < 4)
        return "Цифр в твоем числе меньше, чем нужно. Попробуй еще раз.";
        
    if (arr.length > 4)
        return "Цифр в твоем числе больше, чем нужно. Попробуй еще раз.";
        
    if (n_unique < 4) 
        return "Цифры в числе не должны повторяться. Попробуй еще раз.";
        
    if (arr[0] == 0)
        return "Число не должно начинаться с нуля. Попробуй еще раз.";
    return false;
};